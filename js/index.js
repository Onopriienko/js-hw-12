const images = [];
let i = 0;
let timerID = setInterval(viewImages, 10000);

images[0] = 'img/1.jpg.jpg';
images[1] = 'img/2.jpg.jpg';
images[2] = 'img/3.jpg.JPG';
images[3] = 'img/4.jpg.png';

function viewImages() {
    document.getElementById('image-to-show').src = images[i];
    i++;
    if (i === images.length){
        i = 0;
    }
    setTimeout(timerID);
}
viewImages();

function stop() {
    clearInterval(timerID);
}

function resume() {
    timerID = setInterval(viewImages, 10000);
}

document.getElementById('stop').addEventListener('click', stop);
document.getElementById('resume').addEventListener('click', resume);

